---
title: York169 beamer theme
author: Peter Hill
classoption: aspectratio=169
theme: York169dark
---

# York 16:9 Beamer theme

A LaTeX Beamer theme based on the modern University of York PowerPoint theme,
using 16:9 aspect ratio.

By default, the theme uses the following background images:

1. On the first page, an image with the logo in the top-right and a larger
   version of the coat-of-arms in the background
2. On the last page, a version with the logo in the top-right (light theme), or
   a larger version of the coat-of-arms in the background (dark theme)
3. On all other pages, a blank background

All pages have a small blue band across the top of the page.

# Installing

To use this theme in Linux, place the files in

    ~/texmf/tex/latex/local/beamthemeYork169

You need the following files:

- beamercolorthemeYork169dark.sty
- beamercolorthemeYork169light.sty
- beamerthemeYork169dark.sty
- beamerthemeYork169light.sty
- beamerthemeYork169.sty
- Yorkcolours.sty
- images/YPI_main_page_dark.pdf
- images/YPI_main_page_light.pdf
- images/YPI_main_page_nologo_dark.pdf
- images/YPI_main_page_nologo_light.pdf
- images/YPI_title_page_dark.pdf
- images/YPI_title_page_light.pdf

Mac and Windows users, you're on your own, I'm afraid. See the documentation for
your LaTeX distribution.

# Basic usage

This theme uses images with an aspect ratio of 16:9 for the background, so you
must specify the `aspectratio` option to `\documentclass`:

    \documentclass[aspectratio=169,12pt]{beamer}

There are two themes provided in this package, a light version and a dark
version, named `York169_light` and `York169_dark` respectively. To use, just put
either

    \usetheme{York169light}

or

    \usetheme{York169dark}

in your preamble. See `demo/demo.tex` for a more complete example.

# Pandoc

This theme can easily be used with pandoc for converting e.g. markdown into
beamer presentations. A basic use looks like:

    pandoc example.md -t beamer -V theme:York169dark -V \
      classoption:aspectratio=169 -s -o output.pdf

# Colours

Both themes define the following colours:

- Yorkblue
- Yorkdarkblue
- Yorkaccentblue
- Yorkgrey
- Yorkbeige
- Yorkteal
- Yorkchartreuse
- Yorkgreen
- Yorkyellow
- Yorkorange
- Yorkred
- Yorkmagenta
- Yorkpurple

![York colours][demo/colours-pantones.png]
